<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..', //>>> application directory  
	'name'=> 'TrackStar',
	'id'=> 'TrackStar',
	//'homeUrl'=>'/trackstar/project',
	'homeUrl'=>'/trackstar/project',
	'theme'=>'newtheme',
	'sourceLanguage'=>'en_us', //language the program is written
	'language'=>'zh_cn',  //target language
	//'language'=>'en',		
		
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.modules.admin.models.*', //>>>
	),

	'modules'=>array( //CModule::getModule($id)/setModules
		// uncomment the following to enable the Gii tool
		// http://lee.lh.com/trackstar/index.php?r=gii  >>> as a controller module
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=> '111111', //'Enter Your Password Here'
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*','::1'),
		),
		'admin',

	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		
		// uncomment the following to enable URLs in **path-format**
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'issues'=>'issue/index',
				'issue/<id:\d+>/*'=>'issue/view',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'<pid:\d+>/commentfeed'=>array('comment/feed','urlSuffix'=>'.xml', 'caseSensitive'=>false),
				'commentfeed'=>array('comment/feed','urlSuffix'=>'.xml', 'caseSensitive'=>false),
			),
			'showScriptName'=>false,
		),
		
		'cache'=>array(
				'class'=>'system.caching.CFileCache',
		),
		
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=trackstar',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '111111',
			'charset' => 'utf8',
		),

		'authManager'=>array(
						'class'=>'CDbAuthManager',
						'connectionID'=>'db',
						//>>> 
						'itemTable' =>'tbl_auth_item',
						'itemChildTable' =>'tbl_auth_item_child',
						'assignmentTable' =>'tbl_auth_assignment',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error',
				),
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'info, trace',
					'logFile'=>'infoMessages.log',
				),
		     	array(
					'class'=>'CWebLogRoute',
					'levels'=>'warning',
				),
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
