<?php
/********* funcitons for debug **************/
//short var_dump
function dp()
{
	$arg_list = func_get_args();
	echo '<pre>';
	foreach ($arg_list as $a)
	{
		var_dump($a);
		echo '<br>';
	}
	echo '</pre>';
}

//short var_dump with exit
function dpe()
{
	$arg_list = func_get_args();
	echo '<pre>';
	foreach ($arg_list as $a)
	{
		var_dump($a);
		echo '<br>';
	}
	echo '</pre>';
	exit;
}

//short var_export
function ep()
{
	$arg_list = func_get_args();
	echo '<pre>';
	foreach ($arg_list as $a)
	{
		var_export($a);
		echo '<br>';
	}
	echo '</pre>';
}

//short var_export with exit
function epe()
{
	$arg_list = func_get_args();
	echo '<pre>';
	foreach ($arg_list as $a)
	{
		var_export($a);
		echo '<br>';
	}
	echo '</pre>';
	exit;
}

//debug_backtrace short cut function
function bt()
{
	dpe(debug_backtrace ());
}
/************** shortcut functions **************/
function g( $str )
{
	return isset( $GLOBALS[$str] ) ? $GLOBALS[$str] : false;
}

function t( $str )
{
	return trim($str);
}

function z( $str )
{
	return strip_tags( $str );
}

function c( $str )
{
	if(!empty($_SERVER[$str]))
		return $_SERVER[$str];
	return isset( $GLOBALS['config'][$str] ) ? $GLOBALS['config'][$str] : false;
}

function v($key){
	$value = isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : '');
	if(is_array($value) && sizeof($value) > 0)
	{
		for ($i = 0 ; $i < sizeof($value); $i++)
			$value[$i] = trim($value[$i]);
	}else
		$value = trim($value);
	return $value;
}

function vs($arr){
	$rst = array();
	if(is_array($arr) && sizeof($arr) > 0)
	{
		foreach (array_keys($arr) as $key)
			$rst[$key] = v($key);
	}
	return $rst;
}

/**
 * normalize the unit to M, return the data part
 */
function unit_to_m($v)
{
	preg_match('/\s*([0-9.]+)\s*([a-zA-Z]+)/', $v, $mat);
	$ret = 0;

	if(isset($mat[1]) && isset($mat[2]))
	{
		$unit = strtoupper(t($mat[2]));
		$val  = t($mat[1]);

		if($unit == 'B')
			$ret = $val/1024/1024;
		elseif($unit == 'K' || $unit == 'KB')
		$ret = $val/1024;
		elseif($unit == 'M' || $unit == 'MB')
		$ret = $val;
		elseif($unit == 'G' || $unit == 'GB')
		$ret = $val*1024;
		elseif($unit == 'T' || $unit == 'TB')
		$ret = $val*1024*1024;
		elseif($unit == 'E' || $unit == 'EB')
		$ret = $val*1024*1024*1024;
		else
			return false;
	}
	else
		return false;

	return round($ret, 2);
}

/**
 * format the num data with MB unit
 */
function normal_m_data($data)
{
	$len = strlen($data);

	if($len < 3)
		return intval($data).'M';
	elseif($len < 6)
	return intval($data/1024).'G';
	elseif($len < 9)
	return intval($data/1024/1024).'T';
	else
		return intval($data/1024/1024/1024).'E';
}

/**
 * @desc e.g: unix time to 2years5days45hours53mins42secs
 */
function sec2time($time, $units=null)
{
	if(!$units)
		$units = c('timeunits_en_us');

	if(is_numeric($time))
	{
		$value = array();

		if($time >= 31556926)
		{
			$value["year"] = floor($time/31556926);
			$time = ($time%31556926);
		}

		if($time >= 86400)
		{
			$value["day"] = floor($time/86400);
			$time = ($time%86400);
		}

		if($time >= 3600)
		{
			$value["hour"] = floor($time/3600);
			$time = ($time%3600);
		}

		if($time >= 60)
		{
			$value["minute"] = floor($time/60);
			$time = ($time%60);
		}

		$value["second"] = floor($time);
		$str = '';
		if( isset( $value['year'] ) ) $str .= $value['year'] . $units['year'];
		if( isset( $value['day'] ) ) $str .= $value['day'] . $units['day'];
		if( isset( $value['hour'] ) ) $str .= $value['hour'] . $units['hour'];
		if( isset( $value['minute'] ) ) $str .= $value['minute'] . $units['min'];
		if( isset( $value['second'] ) ) $str .= $value['second'] . $units['sec'];

		return  $str;
	}
	else
		return false;

}

/**
 * @param mixed $sz Bytes unit
 * @return string
 */
function smart_size($sz)
{
	if($sz < 1024)
		return $sz.' Bytes';
	elseif($sz <1048576)
	return round($sz/1024,2).' KB';
	elseif($sz < 1073741824)
	return round($sz/1048576,2).' MB';
	else
		return round($sz/1073741824,2).' GB';
}

/**
 * @param mixed $month Values from 1 to 12 
 * @param mixed $year
 * @return string the days count of a month of a year
 */
function days_in_month($year, $month)
{
	return date('t', mktime(0, 0, 0, $month, 0, $year));
}

/**
 * @return boolean
 * @note: the is just an func example
 */
function is_intranet_ip()
{
	$ip=$_SERVER['REMOTE_ADDR'];
	if(!$ip) return false;

	$available_ips = array(
					array('low' => '10.0.0.0', 'high' => '10.255.255.255'),
					array('low' => '172.16.0.0', 'high' => '172.31.255.255'),
					array('low' => '192.168.0.0', 'high' => '192.168.255.255'),
	);
	$ip=ip2long($ip);
	foreach($available_ips as $v)
	{
		if($ip >= ip2long($v['low']) && $ip <= ip2long($v['high']))
			return true;
	}
	return false;
}

function get_client_ip()
{
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];
	else if($_SERVER['REMOTE_ADDR'])
		return $_SERVER['REMOTE_ADDR'];
	else
		return false;
}

/**
 * @param unknown_type $url
 * @param unknown_type $posts
 * @param unknown_type $headers
 * @param unknown_type $basic
 * @param unknown_type $params
 * @return mixed|string >>> todo log the error rest
 */
function http_request($url,$posts = array(),$headers=array(), $basic = null, $params=null)
{
	$params['cntout'] = isset($params['cntout'])?$params['cntout']:3;
	$params['tout'] = isset($params['tout'])?$params['tout']:15;
	$ch=curl_init();
	curl_setopt($ch,CURLOPT_URL,$url);
	
	if(!empty($posts))
	{
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$posts);
	}

	if($basic)
	{
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, "{$basic['username']}:{$basic['password']}");
	}

	if(!empty($headers)) curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$params['cntout']);
	curl_setopt($ch,CURLOPT_TIMEOUT,$params['tout']);
	$txt=curl_exec($ch);
	curl_close($ch);
	$txt = ltrim($txt,'#');
	$ret=json_decode($txt,true);
	if($ret)
		return $ret;
	return $txt;
}

/**
 * the rule was copied from http://www.10086c.com/index.php/2411.html
 * @return the service provider of the mobile number
 */
function get_mobile_sp($mobile)
{
	$prefix = intval(substr($mobile,0,3));
	if( ($prefix >= 134 && $prefix <= 139)
					|| ($prefix >=187 && $prefix <= 188)
					|| ($prefix == 147 || $prefix == 182)
					|| ($prefix >= 150 && $prefix <= 152)
					|| ($prefix >= 157 && $prefix <= 159)
	) return 'mobile';

	if
	(
					($prefix >= 130 && $prefix <= 132)
					|| ($prefix >= 155 && $prefix <= 156)
					|| ($prefix >= 185 && $prefix <= 186)
					|| ($prefix == 144)
	) return 'unicom';

	if( in_array($prefix,array(133,153,180,189))) return 'telecom';

	return 'unknown';
}

function msubstr($str, $start=0, $length, $charset="utf-8", $suffix="...")
{
	$re['utf-8']  = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
	$re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
	$re['gbk']    = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
	$re['big5']   = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
	preg_match_all($re[$charset], $str, $match);

	if(count($match[0]) <= $length)
		return $str;

	if(function_exists("mb_substr"))
		return mb_substr($str, $start, $length, $charset).$suffix;
	elseif(function_exists('iconv_substr'))
		return iconv_substr($str,$start,$length,$charset).$suffix;

	$slice = implode("",array_slice($match[0], $start, $length));
	return $slice.$suffix;
}

//corresponding to JS escape() func
function unescape($escstr)
{
	preg_match_all("/%u[0-9A-Za-z]{4}|%.{2}|[0-9a-zA-Z.+-_]+/", $escstr, $matches);
	$ar = &$matches[0];
	$c = "";
	
	foreach($ar as $val)
	{
		if (substr($val, 0, 1) != "%")
			$c .= $val;
		elseif (substr($val, 1, 1) != "u")
		{
			$x = hexdec(substr($val, 1, 2));
			$c .= chr($x);
		}
		else
		{
			$val = intval(substr($val, 2), 16);
			if ($val < 0x7F) // 0000-007F
				$c .= chr($val);
			elseif ($val < 0x800) // 0080-0800
			{
				$c .= chr(0xC0 | ($val / 64));
				$c .= chr(0x80 | ($val % 64));
			}
			else // 0800-FFFF
			{
				$c .= chr(0xE0 | (($val / 64) / 64));
				$c .= chr(0x80 | (($val / 64) % 64));
				$c .= chr(0x80 | ($val % 64));
			}
		}
	}
	return $c;
}

/************** log functions *****************/
define('TRACK_LOG', './log.php') ;
function flog($msg, $cat='debug', $logf=TRACK_LOG)
{
	file_put_contents($logf, date('Y-m-d')."::$cat::".str_replace("\n", ' ', $msg)."\n", FILE_APPEND|LOCK_EX );
}

/*************** example functions ************/
function get_signature($name, $version, $expire )
{
	$rip = $_SERVER['REMOTE_ADDR'];
	$sig = hash_hmac('sha256','name:'+$name+'version'+$version+'ip:'+$rip+'expire:'+$expire,'secret_key',true);
	return $sig;
}
