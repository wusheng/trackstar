<?php
$GLOBALS['config']['regx']=array(
				'email' => '/^([0-9a-zA-Z\._-]+)@([0-9a-zA-Z_-]+)(\.([0-9a-zA-Z_-]+)){1,3}$/',
				'uname' => '/^[0-9a-zA-Z_-]{6,18}$/',
				'mobile' => '/^(\+86)?1\d{10}$/',
				'phone' => '/^[0-9\s-]{1,20}$/',
);

$GLOBALS['config']['timeunits_en_us']=array(
				'year'		=> 'years',
				'month'		=> 'mons',
				'day'		=> 'days',
				'hour'		=> 'hours',
				'min'		=> 'mins',
				'sec'		=> 'secs',
);

$GLOBALS['config']['timeunits_zh_cn']=array(
				'year'		=> '年',
				'month'		=> '月',
				'day'		=> '日',
				'hour'		=> '时',
				'min'		=> '分',
				'sec'		=> '秒',
);