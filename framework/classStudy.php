<?php
//class Yii extends YiiBase
$c = new Yii();
$c = new YiiBase();

//class CBehavior extends CComponent implements IBehavior
$c = new CBehavior();
//class CModelBehavior extends CBehavior
$c = new CModelBehavior();
// class CActiveRecordBehavior extends CModelBehavior
$c = new CActiveRecordBehavior();
//class CTimestampBehavior extends CActiveRecordBehavior {
$c = new CTimestampBehavior();
// class CEvent extends CComponent
$c = new CEvent();
// class CComponent
$c = new CComponent();//bbb

//class CWebApplication extends CApplication
$c = new CWebApplication();
//class CConsoleApplication extends CApplication
$c = new CConsoleApplication();
//abstract class CApplication extends CModule
$c = new CApplication();
//abstract class CModule extends CComponent
$c = new CModule();

//class AdminModule extends CWebModule
$c = new AdminModule();
//class CWebModule extends CModule
$c = new CWebModule();
//abstract class CModule extends CComponent
$c = new CModule();

//class ProjectController extends Controller
$c = new IssueController();
//class Controller extends CController
$c = new Controller();
//class CController extends CBaseController
$c = new CController();
//abstract class CBaseController extends CComponent
$c = new CBaseController();

// class COutputProcessor extends CFilterWidget
$c = new COutputProcessor();
// class CContentDecorator extends COutputProcessor
$c = new CContentDecorator();
// class COutputCache extends CFilterWidget
$c = new COutputCache();
// class CFilterWidget extends CWidget implements IFilter
$c = new CFilterWidget();
// class CClipWidget extends CWidget
$c = new CClipWidget();
//class CMenu extends CWidget
$c = new CMenu();
//class CBreadcrumbs extends CWidget
$c = new CBreadcrumbs();
// class CWidget extends CBaseController
$c = new CWidget();

// class Issue extends TrackStarActiveRecord
$c = new Issue();
// abstract class TrackStarActiveRecord extends CActiveRecord
$c = new TrackStarActiveRecord();

// abstract class CActiveRecord extends CModel
$c = new CActiveRecord();
// class CBelongsToRelation extends CActiveRelation
$c = new CBelongsToRelation();
// class CActiveRelation extends CBaseActiveRelation
$c = new CActiveRelation();
// class CBaseActiveRelation extends CComponent
$c = new CBaseActiveRelation();
// class CHasOneRelation extends CActiveRelation
$c = new CHasOneRelation();
// class CHasManyRelation extends CActiveRelation
$c = new CHasManyRelation();
// class CManyManyRelation extends CHasManyRelation
$c = new CManyManyRelation();
// class CHasManyRelation extends CActiveRelation
$c = new CHasManyRelation();
// class CStatRelation extends CBaseActiveRelation
$c = new CStatRelation();
// class CActiveRecordMetaData
$c = new CActiveRecordMetaData();
// class CActiveFinder extends CComponent
$c = new CActiveFinder();
// class CDbCommandBuilder extends CComponent
$c = new CDbCommandBuilder();
// class CDbCriteria extends CComponent
$c = new CDbCriteria();
// class CJoinElement
$c = new CJoinElement();
// class CStatElement
$c = new CStatElement();
// class CJoinQuery
$c = new CJoinQuery();
// abstract class CModel extends CComponent implements IteratorAggregate, ArrayAccess
$c = new CModel();
// class CModelEvent extends CEvent
$c = new CModelEvent();

// class CDbConnection extends CApplicationComponent
$c = new CDbConnection();
// class CDbCommand extends CComponent
$c = new CDbCommand();
// class CDbDataReader extends CComponent implements Iterator, Countable
$c = new CDbDataReader();
// class CDbTransaction extends CComponent
$c = new CDbTransaction();

// class CDbExpression extends CComponent
$c = CDbExpression();

// class CMysqlSchema extends CDbSchema
$c = new CMysqlSchema();
// abstract class CDbSchema extends CComponent
$c = new CDbSchema;
// class CMysqlTableSchema extends CDbTableSchema
$c = new CMysqlTableSchema();
// class CDbTableSchema extends CComponent
$c = new CDbTableSchema();
// class CMysqlColumnSchema extends CDbColumnSchema
$c = new CMysqlColumnSchema();
// class CDbColumnSchema extends CComponent
$c = new CDbColumnSchema();

// class CDbAuthManager extends CAuthManager
$c = new CDbAuthManager();
// class CPhpAuthManager extends CAuthManager
$C = new CPhpAuthManager();
// abstract class CAuthManager extends CApplicationComponent implements IAuthManager
$c = new CAuthManager();

// class CAccessControlFilter extends CFilter
$c = new CAccessControlFilter();
//class CInlineFilter extends CFilter
$c = new CInlineFilter();
// class CFilter extends CComponent implements IFilter
$c = new CFilter();

// class CFilterChain extends CList
$c= new CFilterChain();
//class CList extends CComponent implements IteratorAggregate,ArrayAccess,Countable
$c = new CList();

// class CAssetManager extends CApplicationComponent
$c= new CAssetManager();

// class CPagination extends CComponent
$c = new CPagination();

// class CChoiceFormat
$c = new CChoiceFormat();


//class CNumberFormatter extends CComponent
$c = new CNumberFormatter();
//class CDateFormatter extends CComponent
$c = new CDateFormatter();

//class CPhpMessageSource extends CMessageSource
$c = new CPhpMessageSource();
//abstract class CMessageSource extends CApplicationComponent
$c = new CMessageSource();
//class CStatePersister extends CApplicationComponent implements IStatePersister
$c = new CStatePersister();

// class CUrlManager extends CApplicationComponent
$c = new CUrlManager();
// class CUrlRule extends CBaseUrlRule 
$c = new CUrlRule();
// abstract class CBaseUrlRule extends CComponent
$c = new CBaseUrlRule();

// class CHttpRequest extends CApplicationComponent
$c = new CHttpRequest();
//class CCookieCollection extends CMap
$c = new CCookieCollection();
//class CHttpCookie extends CComponent
$c = new CHttpCookie();
// class CSecurityManager extends CApplicationComponent
$c = new CSecurityManager();

//class CMissingTranslationEvent extends CEvent
$c = new CMissingTranslationEvent();
//class CExceptionEvent extends CEvent
$c = new CExceptionEvent();

//class CFileCacheDependency extends CCacheDependency
$c = new CFileCacheDependency();
//class CCacheDependency extends CComponent implements ICacheDependency
$c = new CCacheDependency();

// class CClientScript extends CApplicationComponent
$c = new CClientScript();
//class CAssetManager extends CApplicationComponent
$c = new CAssetManager();

//class CThemeManager extends CApplicationComponent
$c = new CThemeManager();
// class CTheme extends CComponent
$c = new CTheme();
//class CWidgetFactory extends CApplicationComponent implements IWidgetFactory
$c = new CWidgetFactory();
// abstract class CViewRenderer extends CApplicationComponent implements IViewRenderer
$c = new CViewRenderer();
// class CJavaScript
$c = new CJavaScript();
// class CForm
$c = new CForm();

// class CInlineAction extends CAction
$c = new CInlineAction();
//abstract class CAction extends CComponent implements IAction
$c = new CAction();

// abstract class CTestCase extends PHPUnit_Framework_TestCase
$c = new CTestCase();
// abstract class CWebTestCase extends PHPUnit_Extensions_SeleniumTestCase
$C = new  CWebTestCase();
// abstract class CDbTestCase extends CTestCase
$c = new CDbTestCase();
// class CDbFixtureManager extends CApplicationComponent
$c = new CDbFixtureManager();

/*
 * required: CRequiredValidator
 * filter: CFilterValidator
 * match: CRegularExpressionValidator //ensuring the data matches a regular expression
 * email: CEmailValidator
 * url: CUrlValidator
 * unique: CUniqueValidator
 * compare: CCompareValidator
 * length: CStringValidator
 * in: CRangeValidator
 * numerical: CNumberValidator
 * captcha: CCaptchaValidator
 * type: CTypeValidator
 * file: CFileValidator
 * default: CDefaultValueValidator
 * exist: CExistValidator
 * boolean: CBooleanValidator
 * date: CDateValidator
 * safe: CSafeValidator
 * unsafe: CUnsafeValidator
 */

// abstract class CValidator extends CComponent
$c= new CValidator();

// class CActiveDataProvider extends CDataProvider
$c = new CActiveDataProvider();
//abstract class CDataProvider extends CComponent implements *IDataProvider*
$c = new CDataProvider();

//class CWebUser extends CApplicationComponent implements IWebUser
$c = new CWebUser();

// class UserIdentity extends CUserIdentity
$c = new UserIdentity();
//class CUserIdentity extends CBaseUserIdentity
$c = new CUserIdentity();
//abstract class CBaseUserIdentity extends CComponent implements IUserIdentity
$c = new CBaseUserIdentity();

// class LoginForm extends CFormModel
$c = new LoginForm();
//class CFormModel extends CModel
$c = new CFormModel();

// abstract class CConsoleCommand extends CComponent
$c = new CConsoleCommand();
// class RbacCommand extends CConsoleCommand
$c = new RbacCommand();

// abstract class CInputWidget extends CWidget
$c = new CInputWidget();
// class CAutoComplete extends CInputWidget
$c = new CAutoComplete();
// class CPortlet extends CWidget
$c = new CPortlet();

// class CLocale extends CComponent
$c = new CLocale();
// abstract class CMessageSource extends CApplicationComponent
$c = new  CMessageSource();

// class CLogRouter extends CApplicationComponent
$c = new  CLogRouter();
//class CFileLogRoute extends CLogRoute
$c = new CFileLogRoute();
$c = new CDbLogRoute();
$c = new CEmailLogRoute();
$c = new CWebLogRoute();
$c = new CProfileLogRoute();
// abstract class CLogRoute extends CComponent
$c = new CLogRoute();
// class CErrorHandler extends CApplicationComponent
$c = new  CErrorHandler();
// class CMemCache extends CCache
$c = new CMemCache();
// class CFileCache extends CCache
$c = new CFileCache();
//class CDummyCache extends CApplicationComponent implements ICache, ArrayAccess //>>> This allows you to continue to code to a consistent interface, and when the time comes to actually implement a real caching 
component. 
$c = new CDummyCache();
// abstract class CCache extends CApplicationComponent implements ICache, ArrayAccess
$c = new CCache();

// class CHtml
$c = new CHtml();
// class CFileHelper
$c = new CFileHelper();