<?php
/*
 * performance:
 * >> data cache;
 * >> file based cache;
 * 
 * >>fragment cache
 * >>page cache
 * >>local no cache;
 * >> apc 
 * >>query cache;
 * >> eager loading
 * >> Using yiilite.php
 * >> Enabling schema caching
 * >> Enabling message translation caching
 * 
 * 
 */
/**
 * yii framework adjustion:
 * 1, modify gii to support generate i18 views, and labels; 
 * 2, add some common handy funcs;
 * 
 * yii problems:
 * 1, cron how to use the framework!
 *
 * Study Env: PHP5.4.10; nginx 1.2.4; MySQL 5.1.56 //>>>>>>>>>
 */
####### CH1
>> By default, Yii recognizes URLs with the following format: //>>>
   http://hostname/index.php?r=ControllerID/ActionID
>> Active Record: It maps tables to classes, rows to objects and columns to object attributes.  each instance of an active record class 
represents a single row in a database table.
>> In Yii, a view file belongs to the controller class that rendered it. This way, inside a view script, we can access the controller instance by simply referring to $this. 

####### CH2
## check requirements
>> http://yourhostname/path/to/yii/requirements/index.php 
## Creating a new application // 
% cd WebRoot
% YiiRoot/framework/yiic webapp demo //>>>
% cd demo
% ls –p
assets/    images/    index.php  themes/
css/    index-test.php  protected/

## Creating the controller using yiic
%YiiRoot/framework/yiic shell
%YiiRoot/framework/yiic shell help
CHtml::link("Goodbye",array('message/goodbye') //>>>

####### CH3
## Unit tests
protected/tests/unit/FooTest.php // Foo is the Class name to be tested
Installing PHPUnit: refer to  http://www.phpunit.de/manual/ //todo
$ pear install PEAR-1.9.4
$ sudo pear config-set auto_discover 1
$ sudo pear install pear.phpunit.de/PHPUnit
$ sudo pear install phpunit/PHPUnit_Selenium

## Functional tests
protected/tests/functional/FooTest.php // Foo is the Class name to be tested
Installing Selenium:
1. Download Selenium Remote Control (Selenium RC) zip file from http://seleniumhq.org/download/.
2. Unpack the zip file to a preferred location on your system.
3. cd selenium-remote-control-0.9.2/selenium-server-0.9.2/
% java -jar selenium-server.jar                 //>>>
4. alter protected/tests/WebTestCase.php //>>>
  >>  define('TEST_BASE_URL','http://test-app-domain/index-test.php/'); 
5. alter  protected/tests/phpunit.xml //>>> see it
  >>  <browser name="Internet Explorer" browser="*iexplore" />
% cd protected/tests/
% phpunit functional/SiteTest.php // It will fail if you run site in the vmware

## TDD approach steps:
1). Create the new file, "protected/tests/unit/MessageTest.php" and add to it the following code:
<?php
class MessageTest extends CTestCase
{
}

2). 
%cd /WebRoot/demo/protected/tests
%phpunit unit/MessageTest.php

3). 
class MessageTest extends CTestCase
{
	public function testRepeat()
	{
	}
}
%phpunit unit/MessageTest.php

4).
class MessageTest extends CTestCase
{
	public function testRepeat()
	{
		$message = new MessageController('messageTest');
		$yell = "Hello, Any One Out There?";
		$returnedMessage = $message->repeat($yell);
		$this->assertEquals($returnedMessage, $yell);
	}
}
%phpunit unit/MessageTest.php
5).
Yii::import('application.controllers.MessageController'); //>>> note the bootstrap.php
class MessageTest extends CTestCase
{
	public function testRepeat()
	{
		$message = new MessageController('messageTest');
		$yell = "Hello, Any One Out There?";
		$returnedMessage = $message->repeat($yell);
		$this->assertEquals($returnedMessage, $yell);
	}
}

6).
Add the following method to the MessageController class:
public function repeat($inputString)
{
	return $inputString;
}
%phpunit unit/MessageTest.php

####### CH4
>> simply create a new CDbConnection class and instantiate it:
	$connection=new CDbConnection($dsn,$username,$password);

>> 'components'=>array(
		…
		'db'=>array(
				'connectionString' => 'mysql:host=127.0.0.1;dbname=trackstar_dev', //??? how to use multiple database
				'emulatePrepare' => true,
				'username' => 'your_db_user_name',
				'password' => 'your_db_password',
				'charset' => 'utf8',
		),
),

The emulatePrepare => true configuration sets a PDO attribute (PDO::ATTR_EMULATE_PREPARES) to true which is recommended if
you are using PHP 5.1.3 or higher. //>>>

####### CH5
>> use lowercase for all table names and column names. because ...
>> In order to take full advantage of the integrated table prefix support in Yii, one must appropriately set the 'CDbConnection::tablePrefix' property to be the desired
table prefix. Then, in SQL statements used throughout the application, one can use '{{TableName}}' to refer to table names, where 'TableName' is the name of the table,
but 'without the prefix'. For example, if we were to make this configuration change we could use the following code to query about all projects:
$sql='SELECT * FROM {{project}}';
$projects=Yii::app()->db->createCommand($sql)->queryAll();

>> Configuring Gii
	'modules'=>array(
			'gii'=>array(
					'class'=>'system.gii.GiiModule',
					'password'=>'[add_your_password_here]',
			),
	),
	
>> Adding a required field to our form 
	Array('Attribute List', 'Validator', 'on'=>'Scenario List', …additional options);
	1. One is to specify a method name in the model class itself. "inline Validator"
			/** 
			 * @param string the name of the attribute to be validated 
			* @param array options specified in the validation rule 
			*/
			public function ValidatorName($attribute,$params) { ... }
	
	2. A second is to specify a separate class that is of a Validator type (that is, a class that extends CValidator). "customize seprated Validator Class"
	
	3. The third manner in which you can define the Validator is by specifying a predefined alias to an existing Validator class in the Yii Framework. " predefined Validator alias"
	
>> Fixtures
1. protected/config/test.php
'fixture'=>array(
		'class'=>'system.test.CDbFixtureManager', ),
2.
protected/tests/fixtures/tbl_project.php

>> 
####### CH10
>> A module is similar to an entire mini-application, However, modules cannot be deployed themselves as stand-alone
applications, they must reside within an application.
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 
>> 

